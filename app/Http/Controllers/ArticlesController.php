<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Image;
use Session;
use Auth;


class ArticlesController extends Controller
{
    function showArticles(){
    	$articles = Article::all();
        if(Auth::user()) {
		      return view('articles.articles_list', compact('articles'))->with('articles', $articles);
        }
        else {
            return redirect('login');   
        }
    }

    function show($id){
    	$article = Article::find($id);
    	return view('articles.articles_show_single_item', compact('article'));
    }

    function upload(){
    	return view('articles.articles_upload');
    }

    function store(Request $request){
        $image = $request->file('image');
       	$filename = time() . '.' . $image->getClientOriginalExtension();
    	$new_article = new Article();
    	$new_article->title = $request->title;
        $new_article->content = $request->content;
        $new_article->users_id = Auth::user()->id;
       	$new_article->image = "assets/images/".$filename;
       	Image::make($image)->save(public_path('/assets/images/' . $filename));
       	
        $new_article->save();
    	
        return redirect('articles'); 
    	
    }

    function delete($id){
    	$article_to_delete = Article::find($id);
    	$article_to_delete->delete();
    	Session::flash('delete_notif', "Article Deleted");

        return redirect('articles'); 

    }

    function edit_form($id) {
        $article_tbe = Article::find($id);

        return view('articles.articles_edit_form', compact('article_tbe'));
    }

    function edit($id, Request $request) {
        echo "id: $id";
        $article_tbe = Article::find($id);
        $article_tbe->title = $request->title;
        $article_tbe->content = $request->content;
        $article_tbe->save();

        Session::flash('edit_notif', "Post Edited Successfuly");

        return redirect("articles/$id");
    }    

    function postComment(Request $request, $id) {
    	$comment = $request->description;
    	$user_id = Auth::user()->id;
    	$avatar_id = Auth::user()->avatar;

    	$article_id = $id;
		
		$comment_obj = new \App\Comment;
		$comment_obj->user_id = $user_id;
		$comment_obj->article_id = $article_id;
		$comment_obj->avatar_id = $avatar_id;
		$comment_obj->description = $comment;
		$comment_obj->save();

		return redirect("articles/$id");

    }

     

    
}
