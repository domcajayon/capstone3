<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Comment;
use Session;
use Auth;

class CommentController extends Controller
{
    function save_comment($id, Request $request){
    	$comment = new Comment();
    	$comment->description = $request->description;
    	$comment->article_id = $id;
    	$comment->user_id = Auth::user()->id;
    	$comment->save();

    	return redirect("articles/$id");
    }

    function deleteCom($id){
    	$comment_to_delete = Comment::find($id);
    	$article_id = $comment_to_delete->article->id;
    	$comment_to_delete->delete();
    	// Session::flash('delete_notif', "Article Deleted");

        return redirect("articles/$article_id"); 

    }

    function editCom($id, Request $request) {
    	$comment_tbe = Comment::find($id);
    	$article_id = $comment_tbe->article->id;
    	$comment_tbe->description = $request->content;
    	$comment_tbe->save();

        Session::flash('editcom_notif', "Comment Edited Successfuly!");

        return redirect("articles/$article_id"); 
    }
}
