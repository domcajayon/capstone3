<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profile', 'UserController@profile');

Route::post('/profile', 'UserController@update_avatar');

Route::get('/articles', 'ArticlesController@showArticles');

Route::get('/articles/mypost/{id}', 'UserController@showUserArticles');

Route::get('/articles/upload','ArticlesController@upload');

Route::get('/articles/{id}','ArticlesController@show');

Route::post('/articles/upload', 'ArticlesController@store');

Route::get('/articles/{id}/delete','ArticlesController@delete');

Route::get('/{id}/delete','CommentController@deleteCom');

Route::get('articles/{id}/edit',
	'ArticlesController@edit_form');

Route::post('articles/{id}/edit',
	'ArticlesController@edit');

Route::post('/articles/{id}', 'ArticlesController@postComment');

Route::post('/articles/{id}/editCom', 'CommentController@editCom');		

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
