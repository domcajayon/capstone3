<!DOCTYPE html>
<html>
<head>
	<title>Capstone 3</title>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	  <!-- Compiled and minified JavaScript -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	  
	  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
		<!-- Compiled and minified CSS -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

	  <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

      <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">

      <link rel="canonical" href="/web/tweet-button">

      <link rel="icon" type="image/png" href="{{url('/assets/images/logo9.ico')}}">

      <script>
      	window.twttr = (function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
		if (d.getElementById(id)) return t;
		js = d.createElement(s);
		js.id = id;
		js.src = "https://platform.twitter.com/widgets.js";
		fjs.parentNode.insertBefore(js, fjs);

		t._e = [];
		t.ready = function(f) {
		  t._e.push(f);
		};

  		return t;
		}(document, "script", "twitter-wjs"));
	   </script>

	   <div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10';
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	          
 	<script type="text/javascript">
	
 		
 		$( document ).ready(function(){
 		$(".button-collapse").sideNav();
    $(".dropdown-button").dropdown();

 			
 		});

 		$(document).ready(function(){
	    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
	    $('.modal').modal();
  		});


 		
 	</script>

</head>
<body>
	<div class="col s12">
		
	<div class="navbar-fixed">
	  <nav>
    <div class="nav-wrapper">
      <a href='{{url("articles")}}' class="brand-logo"><img src="" id="laughs">9Laughs</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul id="dropdown1" class="dropdown-content">
  <li><a href='{{url("profile")}}'>My Profile</a></li>
  <li class="divider"></li>
  <li><a href='{{url( "articles/mypost/".Auth::user()->id )}}'>My Posts</a></li>
  
</ul>
      <ul class="right hide-on-med-and-down">

        <!-- Dropdown Trigger -->
      <li><a class="dropdown-button" href="#!" data-activates="dropdown1">My Profile<i class="material-icons right">arrow_drop_down</i></a></li>
        <!-- <li><a href='{{url("profile")}}'>My Profile</a></li> -->
        <li><a href='{{url("articles/upload")}}'>Upload</a></li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>

      </ul>
  </nav>
    </div>
      <ul class="side-nav" id="mobile-demo">
        <li><a href='{{url("profile")}}'>My Profile</a></li>
        <li><a href='{{url( "articles/mypost/".Auth::user()->id )}}'>My Posts</a></li>
        <li><a href='{{url("articles/upload")}}'>Upload</a></li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>

      </ul>
    </div>
  <main>
  	<div class="row">

  		<div class="col s12 m2">
  			<div class="side">
  				
  			</div>
  		</div>

  		<div class="col s12 m7">
  			
 	<div>
 		<h1></h1>
 	</div>
 	<div class="marg">
 		<!-- <h2>Content:</h2> -->
 		
 		@yield("main_content")
 		
 	</div>
  		</div>

      <div class="col s12 m1">
        
      </div>


  		<div class="col s12 m2">
  			<div class="sidecard col s12 m2">
  				
  			<div class="card hoverable">
            	<div class="card-image">
              	<img src="{{url('/assets/images/sidebar.jpg')}}">
              	<!-- <span class="card-title"></span> -->
            	</div>
            	<div class="card-content teal lighten-2">
              	<p>“There is nothing in the world so irresistibly contagious as laughter and good humor.” <br>― Charles Dickens</p>
            	</div>
          	</div>

          	<div class="card hoverable">
            	<div class="card-image">
              	<img src="{{url('/assets/images/laugh.jpg')}}">
            	</div>
            	<div class="card-content cyan accent-3">
              	<p>“Always laugh when you can, it is cheap medicine.” 
					<br>― George Gordon Byron</p>
            	</div>
          	</div>
  			
  			</div>
  		</div>


  	
  </main>
  <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col m5 s12">
                <h5 class="white-text">About 9 Laughs</h5>
                <p class="grey-text text-lighten-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
              </div>
              <div class="col offset-m1 m6 s12">
                <h5 class="white-text">Laugh with Us</h5>
                <i class="small material-icons">location_on</i>
                <span>Caswynn Building, No. 134 Timog Avenue, Quezon City</span>
                <br>
                <i class="small material-icons">phone</i>
                <span>+63 922 7770875</span>
                <br>
                <i class="small material-icons">mail</i>
                <a href=""><span>domzofficial@9laughs.com</span></a>
              </div>
              <div class="col m12 s12">
              	
                <div class="center-align">
       	        	 <a href="https://www.facebook.com/domcajayon" class="fa fa-facebook"></a>
			         <a href="https://www.twitter.com" class="fa fa-linkedin"></a>
			         <a href="https://www.google.com" class="fa fa-google"></a>
			         <a href="https://www.youtube.com" class="fa fa-youtube"></a>
			         <a href="https://www.instagram.com" class="fa fa-instagram"></a>
	        	</div>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container center-align">
            	<strong>Copyright &copy; 2017 | Dominic D. Cajayon. All Rights Reserved.</strong>
            </div>
          </div>
        </footer>
	</div>

</body>
</html>