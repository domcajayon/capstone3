@extends('applayout')

@section('main_content')

		@if (Session::has('edit_notif'))
<div class="card-panel green lighten-1">
		{{ Session::get('edit_notif') }}
</div>
		@endif

<h3>{{ $article->title }}</h3>
<div class="postborder">
<img class="articlepost" src='{{asset("$article->image")}}'>
<br>
<div class="left-align">
	      <button class="btn blue"><i class="material-icons">arrow_upward</i></button>
	      <button class="btn red"><i class="material-icons">arrow_downward</i></button>
	
</div>
<div class="right-align shares">

		<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>

	<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Hello%20world" data-size="large">Tweet</a>
	
</div>
</div>

<br>
<span>{{$article->comments->count()}} Comments</span>
<hr>

@foreach($article->comments as $comment)

			<div class="komento">	
			@if(Auth::user()== $comment->get_user)
			<div class="right-align eddel">
				
			<a class='dropdown-button' href='#' data-activates='dropdown{{$comment->id}}'><i class="material-icons">settings</i></a>
			</div>

			  <!-- Dropdown Structure -->
			  <ul id='dropdown{{$comment->id}}' class='dropdown-content'>
			    <li><a href="" data-target="modal{{$comment->id}}" class="modal-trigger">Edit</a></li>
			    <li><a href='{{ url("$comment->id/delete") }}'>Delete</a></li>
			  </ul> 

			   <!-- Modal Structure -->
			  <div id="modal{{$comment->id}}" class="modal">
			    	<form method="POST" action="/articles/{{$comment->id}}/editCom" class="form-horizontal">
			    <div class="modal-content">
			    		{{ csrf_field() }}
			    		
			      	<h4>Edit Comment</h4>
			      	<label>Content: </label><br>
					<textarea class="form-control" name="content">{{ $comment->description }}</textarea><br>
			    </div>
			    <div class="modal-footer">
			      	<a href="#!" class="modal-action modal-close waves-effect waves-green btn cyan lighten-1">Cancel</a>
					<button class="waves-effect btn green accent-2"><input type="submit" value="edit"></button>
			    </div>
			    	</form>
			  </div>
	



			 @else
			 
			 <div class="right-align eddel">
			 	<!-- <span>N/A</span> -->
			 </div> 

			 @endif

			<img class="box6" src="/assets/avatars/{{ $comment->avatar_id }}">
			<strong>{{ $comment->get_user->name }}</strong> 
			<p class="margincomm">{{ $comment->description }}</p>
			<div class="likerep">
				
			<a href=""><span>Like</span></a>
			<span>·</span>
			<a href="#textarea1"><span>Reply</span></a>
			{{ $comment->updated_at }} <br>
			
			</div>
			</div>
							
@endforeach

@if (Session::has('editcom_notif'))
<div class="card-panel green lighten-1">
		{{ Session::get('editcom_notif') }}
</div>
		@endif

<div class="commentform card-panel grey lighten-3">
	
<form action="" method="POST">
	{{ csrf_field() }}
	<span>Comment: </span>
	<br>
	<textarea id="textarea1" name='description' placeholder="Write a comment..."></textarea><br>
	<div class="right-align">
		
	<input class="btn green" type="submit" value="Add Comment">	
	</div>
</form>
</div>


@endsection