@extends('applayout')

@section('main_content')
	<h2>Edit Post:</h2>
	<div class="row">
		
	<div class="col-md-6">
		
	<form method="POST" class="form-horizontal">
		{{ csrf_field() }}
		<label>Title: </label><br>
		<input type="text" class="form-control" name="title" value="{{$article_tbe->title}}" placeholder="Enter Title"><br>
		
		<img class="editpost" src='{{asset("$article_tbe->image")}}'>
		<br>
		<label>Content: </label><br>
		<textarea class="form-control" name="content">{{$article_tbe->content}}</textarea><br>

		<input class="btn blue" type="submit" name="create" value="Post">
	</form>
	</div>
	</div>
@endsection