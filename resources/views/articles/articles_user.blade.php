@extends('applayout')

@section('main_content')

<h2>Your Funny Posts</h2>
<ul>
	
@foreach($articles as $article)

	<div class="postborder">
	<li>
		<a class="aimage" href= '{{url( "articles/$article->id" )}}'>{{ $article->title }}
		<br>
		<img class="articlepost hoverable" src='{{asset("$article->image")}}'>
		</a>	
	</li>

	<div>
		
 		<span>{{$article->comments->count()}} Comments</span>
		
	</div>

	<div class="row">
		<div class="left-align">
	      <button class="btn blue"><i class="material-icons">arrow_upward</i></button>
	      <button class="btn red"><i class="material-icons">arrow_downward</i></button>
	      <a class="btn green" href='{{url( "articles/$article->id" )}}'><i class="material-icons">comment</i></a>
		</div>

		<div class="right-align shares">
			
		
	<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>

	<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Hello%20world" data-size="large">Tweet</a>
		</div>

<div>
	<div class="deled">
		
	<a href='{{ url("articles/$article->id/edit")}}' class="btn blue darken-3">Edit Post</a>

	 <button data-target="modal1" class="btn red accent-2 modal-trigger">Delete</button>
	</div>

	   <!-- Modal Structure -->
	  <div id="modal1" class="modal">
	    <div class="modal-content">
	      <h4>Delete Post</h4>
	      <p>Are you sure you want to delete this post?</p>
	    </div>
	    <div class="modal-footer">
	      	<a href="#!" class="modal-action modal-close waves-effect waves-green btn cyan lighten-1">Cancel</a>
			<a class="waves-effect btn red accent-2" href='{{ url("articles/$article->id/delete") }}'>Delete</a>
	    </div>
	  </div>
</div>

	</div>
	</div>
	<hr>

@endforeach
</ul>

@endsection