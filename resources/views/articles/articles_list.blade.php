@extends('applayout')


@section('main_content')
<!-- <h3>List of Articles</h3> -->
<ul>
		
	@foreach($articles->sortByDesc('updated_at') as $article)
	<div class="postborder">
	<li>
		<a class="aimage" href= '{{url( "articles/$article->id" )}}'>{{ $article->title }}
		<br>
		<img class="articlepost hoverable" src='{{asset("$article->image")}}'>
		</a>	
	</li>

	<div>
		
 		<span>{{$article->comments->count()}} Comments</span>
		
	</div>

	<div class="row">
		<div class="left-align">
	      <button class="btn blue"><i class="material-icons">arrow_upward</i></button>
	      <button class="btn red"><i class="material-icons">arrow_downward</i></button>
	      <a class="btn green" href='{{url( "articles/$article->id" )}}'><i class="material-icons">comment</i></a>
		</div>

		<div class="right-align shares">
			
		
	<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>

	<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Hello%20world" data-size="large">Tweet</a>
		</div>
	</div>
	</div>
	<hr>
	@endforeach
</ul>
@endsection