@extends('applayout')

@section('main_content')

<h1>Upload a funny post</h1>

<form enctype="multipart/form-data" action="" method="POST">
	{{ csrf_field() }}
	<span>Title:</span> <input type="text" name="title"> <br>
	<span>Content:</span> <br>
	<textarea name="content"></textarea><br>
	<div class="file-field input-field">
      <div class="waves-effect btn light-green lighten-2">
        <span>File</span>
        <input type="file" name="image">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
	<input class="waves-effect btn" type="submit" name="">
</form>

@endsection