@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="/assets/avatars/{{ $user->avatar }}" style="width: 150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px;">
            <h2>My Profile</h2>
            <h3>Name: {{$user->name }}</h3>
            <h3>Email: {{$user->email }}</h3>
            <form enctype="multipart/form-data" action="/profile" method="POST">
                <span>Update Profile Image:</span>
                <input type="file" name="avatar" class="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br>
                <input type="submit" class="waves-effect btn btn-success">
            </form>
        </div>
    </div>
</div>
@endsection
